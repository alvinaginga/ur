@extends('layouts/default')

{{-- Page title --}}
@section('title')
Intercessory
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/aboutus.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/animate/animate.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/devicon/devicon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/devicon/devicon-colors.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-sm-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">About Us</a>
                </li>
            </ol>
            <div class="float-right breadcrum_adjust">
                <i class="livicon icon3" data-name="users" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> About Us
            </div>
        </div>
    </div>

        </div>
    </div>
    @stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <!-- Slider Section Start -->
        <div class="row my-3">
            <!-- Left Heading Section Start -->
            <div class="col-md-7 col-sm-12  col-md-12 col-lg-8 wow bounceInLeft" data-wow-duration="5s">
                <h2><label>Welcome to Intercessory</label></h2>
                <p>
                  Jeremiah 29:13. You will seek me and find me when you seek me with all your heart. Prayer is as essential as breathing and to neglect it must result in weakness and defeat .It is not only important but it is of supreme importance.
                  The Church recognizes that the church is built upon the foundation of Apostles and Prophets and that God sends his Apostles and Prophets to pioneer every new thing he desires to do on earth. God send Apostle Das to pioneer this apostolic move that is going to sweep the entire world in preparation for Christ’s return.
                </p>
            </div>
            <!-- //Left Heaing Section End -->
            <!-- Slider Start -->
            <div class="col-md-12 col-sm-12  col-lg-4 slider wow fadeInRightBig" data-wow-duration="5s">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints1.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints2.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints3.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints4.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints5.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints6.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints7.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints8.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/intercessory_images/ints9.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au10.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au11.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au12.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au13.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                </div>
            </div>
            <!-- //Slider End -->
        </div>
        <!-- //Slider Section End -->
        <!-- Services Section Start -->
        <div class="text-center">
            <h3 class="border-success"><span class="heading_border bg-success" >What to expect</span></h3>
        </div>
        <div class="row">
            <!-- left Section Start -->
            <div class="col-md-12 col-sm-12 col-lg-6 col-12">
                <div class="row">
                <!-- Responsive Section Start -->
                <div class="col-sm-6 col-md-12 col-lg-6 col-12 wow zoomIn" data-wow-duration="3s">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon1" data-name="desktop" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <div class="info">
                            <h3 class="success text-center mt-3">Call to interceed</h3>
                            <p>There has never been a spiritual awakening (revival) in any country or locality that didn’t begin in united prayer. Intercession is a key pillar in the body of Christ and one of the pillar the ministry(FLYM Ministry as well as Chrisco Upperroom church).</p>
                            <div class="text-right primary"><a href="#">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Responsive Section End -->
                <!-- Easy to Use Section Start -->
                <div class="col-sm-6 col-md-12 col-lg-6 col-12 wow zoomIn" data-wow-duration="3s">
                    <div class="box">
                        <div class="box-icon box-icon1">
                            <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#418bca" data-hc="#418bca"></i>
                        </div>
                        <div class="info">
                            <h3 class="primary text-center mt-3">Persisting in battle</h3>
                            <p>Intercessory prayer is also prayer that doesn't give up. It's the kind of prayer that endures all setbacks and overcomes every obstacle. It's prayer that "presses on" until we "apprehend" God's will in whatever situation we are facing (see Phil. 3:12, KJV).</p>
                            <div class="text-right primary"><a href="#">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Easy to use Section End -->
            </div>
            </div>
            <!-- Left Section End -->
            <div class="col-md-12 col-sm-12 col-lg-6 col-12 wow bounceInRight" data-wow-duration="3s">
                <!-- Pnael group section Start -->
                <div class="margin-t30 d-none d-md-block"></div>
                <div  id="accordion">
                    <div class="card card_collapse">
                        <div class="card-header header_card" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-minus success"></i>
                                    <span class="success">What is Intercession</span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="media">
                                <div class="media-top">
                                    <a href="#">
                                    <!--    <i class="devicon-php-plain colored font100" ></i> -->
                                    </a>
                                </div>
                                <div class="media-body">
                                    <p class="media-heading">
                                        Intercession is prayer that pleads with God for your needs and the needs of others. But it is also much more than that. Intercession involves taking hold of God's will and refusing to let go until His will comes to pass.
                                        Intercession is warfare -- the key to God's battle plan for our lives. But the battleground is not of this earth. The Bible says, "We are not fighting against humans. We are fighting against forces and authorities and against rulers of darkness and spiritual powers in the heavens above" (Eph. 6:12).
                                        Intercessory prayer takes place in this spiritual world where the battles for our own lives, our families, our friends and our nation are won or lost.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Php Section End -->
                    <!-- Html Section Start -->
                    <div class="card card_collapse">
                        <div class="card-header header_card" id="headingtwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-plus success"></i>
                                    <span class="success">A plan for battle</span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapsetwo" class="collapse" aria-labelledby="headingtwo" data-parent="#accordion">
                            <div class="card-body">
                                <div class="media">
                                <div class="media-left media-top">
                                <a href="#">
                                <!-- <i class="devicon-html5-plain colored font100"></i> -->
                                </a>
                                </div>
                                <div class="media-body">
                                <p class="media-heading">
                                    If you are born again, you are God's son or daughter (John 1:12). As His child, you have a direct "hotline" to God. At any time, you can boldly come into His presence (Heb. 4:16)
                                    This incredible access to God is the basis for intercession. Once you are in God's presence, you can now discover His battle plan for the situation you are facing. Because prayer alone is not enough -- you need a target for your prayers!
                                    To discover God's plan, all you have to do is ask. The Bible says that "if any of you need wisdom, you should ask God, and it will be given to you" (James 1:5). When we ask God for wisdom, His desires will become the focus of our prayers.
                                    "Let God change the way you think. Then you will know how to do everything that is good and pleasing to Him" (Romans 12:2).
                                </p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card_collapse">
                        <div class="card-header header_card" id="headingthree">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapsethree" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-plus success"></i>
                                    <span class="success">Armed for battle</span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapsethree" class="collapse" aria-labelledby="headingthree" data-parent="#accordion">
                            <div class="card-body">
                            <div class="media">
                            <div class="media-left media-top">
                            <a href="#">
                           <!-- <i class="devicon-jquery-plain colored font100" ></i> -->
                            </a>
                            </div>
                            <div class="media-body">
                            <p class="media-heading">
                                Intercessory prayer is a serious matter. And just like soldiers who are preparing for battle, we cannot take on the enemy if we leave our weapons behind. That's why we must go into "battle" armed for spiritual conflict (see 2 Cor. 10:3,4).
                                First, recognize that Jesus is in control of the situation. Jesus "rules over forces, authorities, powers, and rulers ... over all beings in this world and will rule in the future world as well" (Eph. 1:21). He is King of Kings and Lord of Lords. Then, put on "all the armor God gives" (see Eph. 6) so that you will be ready to fight with God's weapons. These are the "weapons of our warfare" that can pull down strongholds in the spirit world (see 2 Cor. 10:3,4). They will also protect you from the attacks that are sure to come once you begin the spiritual battle.
                                Next, bind the work of Satan, knowing that Jesus has given you authority "to defeat the power of your enemy" (Luke 10:19). If God shows you the identity of specific spiritual strongholds that are at work, take authority over these strongholds in the name of Jesus. And always remember that "God's Spirit is in you and is more powerful that the one that is in the world" (1 John 4:4).
                                Finally, as you begin the spiritual battle, take comfort knowing that you are not alone: Jesus also is interceding on your behalf! The Bible says that Jesus "is able to save forever those who draw near to God through Him, since He always lives to make intercession for them" (Heb. 7:25, NASB; see also Romans 8:26,27,34).
                            </p>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Html Section End -->
                    <!-- Jquery Section Start -->
                    <!--<div class="card card_collapse">-->
                        <!--<div class="card-heading text_bg">-->
                            <!--<h4 class="card-title">-->
                                <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">-->
                                    <!--<span class="fa fa-plus success"></span>-->
                                <!--<span class="success">JQUERY</span></a>-->
                            <!--</h4>-->
                        <!--</div>-->
                        <!--<div id="collapseThree" class="panel-collapse collapse">-->
                            <!--<div class="card-body">-->
                                <!--<div class="media">-->
                                    <!--<div class="media-left media-top">-->
                                        <!--<a href="#">-->
                                            <!--<i class="devicon-jquery-plain colored font100" ></i>-->
                                        <!--</a>-->
                                    <!--</div>-->
                                    <!--<div class="media-body">-->
                                        <!--<p class="media-heading">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>-->
                                    <!--</div>-->
                                <!--</div>-->
                            <!--</div>-->
                        <!--</div>-->
                    <!--</div>-->
                    <!-- //Jquery Section End -->
                </div>
                <!-- //Panel group Section End -->
            </div>
        </div>
        <!-- // Services Section End -->
        <!-- Our Team Section Start -->
        <div class="text-center my-3">
            <h3 class="border-danger"><span class="heading_border bg-danger" >Intercessory Gallery</span></h3>
        </div>
        <div class="row">
            <!-- Our Team Heading Start -->
            <!-- //Our Team Heading End -->
            <!-- Image1 Section Start -->
            <div class="col-md-6 col-sm-12 col-12  col-lg-3 profile wow zoomIn" data-wow-duration="3.5s">
                <div class="thumbnail bg-white  text-center">
                    <img src="{{ asset('assets/images/interpic1.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Elders laying on hands</b>
                    
                        <div class="divide">
                            <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="https://twitter.com/CUpperRoom"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="https://plus.google.com/112218850217091238784"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image1 Section End -->
            <!-- Image2 Section Start -->
            <div class="col-md-6 col-sm-12 col-12 col-lg-3 profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white text-center">
                    <img src="{{ asset('assets/images/interpic2.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Pastor Paul leading in deliverance</b>
                        <div class="divide">
                            <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="https://twitter.com/CUpperRoom"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="https://plus.google.com/112218850217091238784"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image2 Section End -->
            <!-- Image3 Section Start -->
            <div class="col-md-6 col-sm-12 col-12 col-lg-3  profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white  text-center">
                    <img src="{{ asset('assets/images/interpic3.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Soaking prayer</b>
                        <div class="divide">
                            <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="https://twitter.com/CUpperRoom"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="https://plus.google.com/112218850217091238784"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Image3 Section End -->
            <!-- Image4 Section Star -->
            <div class="col-md-6 col-sm-5 col-12  col-lg-3 profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white text-center">
                    <img src="{{ asset('assets/images/interpic4.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Total surrender</b>
                        
                        <div class="divide">
                            <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="https://twitter.com/CUpperRoom"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="https://plus.google.com/112218850217091238784"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd" data-hc="#1b86bd"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image4 Section End -->
        </div>
        <!-- //Our Team Section End -->
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('assets/vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/wow/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/aboutus.js') }}"></script>
    <!--page level js ends-->
@stop
