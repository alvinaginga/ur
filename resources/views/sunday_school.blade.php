@extends('layouts/default')

{{-- Page title --}}
@section('title')
Worship | Chrisco Upperroom
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/cart.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/tabbular.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-rating/bootstrap-rating.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">


            <ol class="breadcrumb">
                <li  class="breadcrumb-item">
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-sm-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">Sunday School</a>
                </li>
            </ol>
            <div class="float-right breadcrum_adjust">
                <i class="livicon icon3" data-name="edit" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Sunday School
            </div>
        </div>
    </div>
        </div>
    </div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container my-3">
        <!--item view start-->
        <div class="row">
            <!--<div class="mart10">-->
                <!--product view-->
                <div class="col-md-4 col-lg-4 col-sm-6 col-12">
                    <div class="row">
                        <div class="product_wrapper">
                            <img id="zoom_09" src="{{ asset('assets/images/sunday/small/sss1.jpg') }}" data-zoom-image="{{ asset('assets/images/sunday/big/ssb1.jpg') }}" class="img-fluid" />
                        </div>
                    </div>
                    <div class="row">
                        <!--individual products in product view-->
                        <div id="gal1">
                            <a href="#" data-image="{{ asset('assets/images/sunday/small/sss1.jpg') }}" data-zoom-image="{{ asset('assets/images/sunday/big/ssb1.jpg') }}">
                                <img id="img_01" src="{{ asset('assets/images/sunday/small/sss1.jpg') }}" class="img-fluid" />
                            </a>
                            <a href="#" data-image="{{ asset('assets/images/sunday/small/sss2.jpg') }}" data-zoom-image="{{ asset('assets/images/sunday/big/ssb2.jpg') }}">
                                <img id="img_01" src="{{ asset('assets/images/sunday/small/sss2.jpg') }}" class="img-fluid" />
                            </a>
                            <a href="#" data-image="{{ asset('assets/images/sunday/small/sss3.jpg') }}" data-zoom-image="{{ asset('assets/images/sunday/big/ssb3.jpg') }}">
                                <img id="img_01" src="{{ asset('assets/images/sunday/small/sss3.jpg') }}" class="img-fluid" />
                            </a>
                            <a href="#" data-image="{{ asset('assets/images/sunday/small/sss4.jpg') }}" data-zoom-image="{{ asset('assets/images/sunday/big/ssb4.jpg') }}">
                                <img id="img_01" src="{{ asset('assets/images/sunday/small/sss4.jpg') }}" class="img-fluid" />
                            </a>
                        </div>
                    </div>
                </div>
                <!--individual product description-->
                <div class="col-md-8 col-sm-6 col-lg-8 col-12 ml-auto">
                    <h2 class="text-primary">Chrisco Upperroom Sunday School </h2>
                    <p>Proverbs 22 :6 Train up a child in the way he should go: and when he is old, he will not depart from it. Mathew 19:14 "....suffer little children, and forbid them not, to come unto me: for such is the Kingdom of heaven."
                       Eph 6:1-4 Children, obey your parents in the Lord: for this is right. Honour thy father and mother; (which is the first commandment with promise;) That it may be well with thee, and thou mayest live long on the earth.
                       And, ye fathers, provoke not your children to wrath: but bring them up in the nurture and admonition of the Lord.
                    </p>        
                </div>
            </div>
        <!--</div>-->
        <!--item view end-->
        <!--item desciption start-->
        <div class="row">
            <div class="col-sm-12">
                <!-- Tabbable-Panel Start -->
                <div class="tabbable-panel">
                    <!-- Tabbablw-line Start -->
                    <div class="tabbable-line">
                        <!-- Nav Nav-tabs Start -->
                        <ul class="nav nav-tabs ">
                            <li class="nav-item ">
                                <a href="#tab_default_1" data-toggle="tab" class="nav-link active">
                                Description </a>
                            </li>
                        </ul>
                        <hr class="horizontalline">
                        <!-- //Nav Nav-tabs End -->
                        <!-- Tab-content Start -->
                        <div class="tab-content">
                                <div class="tab-pane active show fade" id="tab_default_1">
                                <p>Sunday school ministry caters for the spiritual nourishment of children. It is composed of teachers, teens and the younger children. We meet on Sundays for 1 to 1.5 hours as from 12:00 noon onwards.
                                  We teach on the word of God to grow their spiritual lives, pray with children as led by the Spirit, teach on real live issues e.g Hygiene, discipline, responsibility, accountability and relating with other people. From time to time we hold activities for the children such as seminars, hikes and plays.
                                  </p>
                                <ul>
                                    <li><i class="livicon" data-name="check" data-size="18" data-loop="true" data-c="#555555" data-hc="#555555"></i> Develop a Passionate Relationship with God</li>
                                    <li><i class="livicon" data-name="check" data-size="18" data-loop="true" data-c="#555555" data-hc="#555555"></i>Training the Child and bringing them up in the way of the Lord</li>
                                    <li><i class="livicon" data-name="check" data-size="18" data-loop="true" data-c="#555555" data-hc="#555555"></i>Teaching Children to reverence God</li>
                                    <li><i class="livicon" data-name="check" data-size="18" data-loop="true" data-c="#555555" data-hc="#555555"></i>Listening and understanding the children</li>
                                </ul>
                                <p>This ministry is under the leadership of Pastor Joan assisted by Sunday school teachers. Our teachers are trained from time to time to handle the work of God in Sunday school ministry. We have four groupings namely A, B, C and D according to their ages.</p>
                        
                            </div>
                            <!-- Tab-content End -->
                        </div>
                        <!-- //Tabbable-line End -->
                    </div>
                    <!-- Tabbable_panel End -->
                </div>
            </div>
        </div>
        <!--item desciption end-->
        <!--recently view item-->
        <h2 class="text-primary"> Children Gallery</h2>
        <div class="divider"></div>
        <div class="row">
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/sunday/default/ssd1.jpg') }}" alt="children image" class="img-fluid">
                    <figcaption>
                        <h4 class="text-white">Mama Das praying for the children</h4>
                        <ul class="hidden-xs">
                            <li>KICC, Amphitheatre</li>
                            <li>Sunday service</li>
                        </ul>
                    </figcaption>
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/sunday/default/ssd2.jpg') }}" alt="children image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/sunday/default/ssd3.jpg') }}" alt="children image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/sunday/default/ssd4.jpg') }}" alt="children image" class="img-fluid">
                </figure>
            </div>
   
        </div>
        <!--recently view item end-->
    </div>
    <!-- //Container Section End -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!--page level js start-->
    <script type="text/javascript" src="{{ asset('assets/js/frontend/elevatezoom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-rating/bootstrap-rating.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/cart.js') }}"></script>
    <!--page level js start-->

@stop
