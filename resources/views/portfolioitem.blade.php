@extends('layouts/default')

{{-- Page title --}}
@section('title')
Portfolio_Item
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/portfolio.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/animate/animate.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.theme.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-sm-block breadcrumb-item">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">Portfolio Item</a>
                </li>
            </ol>
            <div class="float-right breadcrum_adjust">
                <i class="livicon icon3" data-name="clip" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Portfolio Item
            </div>
        </div>
    </div>
        </div>
    </div>
    @stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container my-3">
        <h2 id="single_portfolio_title"><label> Single Portfolio</label></h2>
        <div class="row details">
            <!-- Slider Section Start -->
            <div class="col-md-12  col-lg-6 col-12 wow bounceInLeft" data-wow-duration="1.5s">
                <div class="row">
                <div class="col-md-12 col-sm-12 col-lg-12 slider">
                    <div id="owl-demo" class="owl-carousel owl-theme">
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa61.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa62.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa63.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa64.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa65.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa66.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa67.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa68.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa69.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                        <div class="item"><img src="{{ asset('assets/images/gallery/ssa70.jpg') }}" alt="slider-image" class="img-fluid">
                        </div>
                    </div>
                </div>
            </div>
            </div>
            <!-- //Slider Section End -->
            <!-- Project Description Section Start -->
            <div class="col-md-12 col-lg-6 col-12 wow bounceInRight" data-wow-duration="1.5s">
                <h3 class="project">We belong to God</h3>
                <p class="text-justify">
                   Isaiah 43:1-2
                   "But now, this is what the Lord says – he who created you, Jacob, he who formed you, Israel: 'Do not fear, for I have redeemed you; I have summoned you by name; you are mine. When you pass through the waters, I will be with you; and when you pass through the rivers they will not sweep over you. When you walk through the fire, you will not be burned; the flames will not set you ablaze.'" 
                </p>
                <h3>Client Details</h3>
                <ul style="padding: 0 0 0 10px;">
                    <li><b>Category:</b>&nbsp;Chrisco</li>
                    <br />
                    <li><b>Client:</b>&nbsp;Upperroom</li>
                    <br />
                    <li><b>Tags:</b>&nbsp;Photo, Gallery</li>
                    <br />
                    <li><b> Link:</b><a href="#">&nbsp;www.chriscoupperroom.org</a></li>
                    <br />
                    <li><a class=" btn btn-primary" href="#"><span class="text-white"><i class="livicon" data-name="hand-right" data-size="24" data-loop="true" data-c="#fff" data-hc="white"></i></span></a></li>
                </ul>
            </div>
            <!-- //Project Description Section End -->
        </div>
        <!-- Related Section Start -->
        <div class="text-center">
            <h3 class="border-success"><span class="heading_border bg-success">Additional Gallery</span></h3>
        </div>
        <div class="row">
            <!--<div class="col-md-12 col-lg-12 col-12 project_images">-->
                <div class="col-md-6 col-lg-3 col-12   project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa71.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa72.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa73.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa74.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa75.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa76.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa77.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa78.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa79.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa80.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa81.jpg') }}" class="img-fluid"></a>
                </div>
                <div class="col-md-6 col-lg-3 col-12  project_images">
                    <a href="#"><img src="{{ asset('assets/images/gallery/ssa82.jpg') }}" class="img-fluid"></a>
                </div>
            </div>
        </div>
        <!-- Related Setion End -->
    <!--</div>-->
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('assets/vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/wow/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/carousel.js') }}"></script>
    <!--page level js ends-->
@stop
