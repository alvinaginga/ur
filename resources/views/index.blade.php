@extends('layouts/default')

{{-- Page title --}}
@section('title')
Home
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
<!--page level css starts-->
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/tabbular.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/animate/animate.min.css') }}"/>
<link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/jquery.circliful.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.theme.css') }}">

<style>
    .box{
        margin-top:53px !important;
    }

</style>

<!--end of page level css-->
@stop

{{-- slider --}}
@section('top')
<!--Carousel Start -->
<div id="owl-demo" class="owl-carousel owl-theme">
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide1.jpg') }}" alt="slider-image"/>
    </div>
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide2.jpg') }}" alt="slider-image">
    </div>
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide3.jpg') }}" alt="slider-image">
    </div>
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide4.jpg') }}" alt="slider-image">
    </div>
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide5.jpg') }}" alt="slider-image">
    </div>
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide6.jpg') }}" alt="slider-image">
    </div>
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide7.jpg') }}" alt="slider-image">
    </div>
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide8.jpg') }}" alt="slider-image">
    </div>
    <div class="item img-fluid"><img src="{{ asset('assets/images/uslide9.jpg') }}" alt="slider-image">
    </div>
</div>
<!-- //Carousel End -->
@stop

{{-- content --}}
@section('content')
<div class="container">
    <section class="purchas-main">
        <div class="container bg-border wow pulse" data-wow-duration="2.5s">
            <div class="row">
                <div class="col-md-7 col-sm-7 col-12 col-lg-8">
                    <h1 class="purchae-hed mt-3">CHRISCO UPPERROOM FELLOWSHIP</h1></div>
                <div class="col-md-5 col-sm-5 col-12 col-lg-4"><a href="#" class="btn purchase-styl float-lg-right">HOME PAGE
                    </a></div>
            </div>
        </div>
    </section>
    <!-- Service Section Start-->
    <div class="row">
        <!-- Responsive Section Start -->
        <div class="col-12 text-center my-3">
            <h3 class="border-primary"><span class="heading_border bg-primary mx-auto">What to expect</span></h3></div>
        <div class="col-sm-6 col-md-6 col-lg-3 wow bounceInLeft" data-wow-duration="3.5s">
            <div class="box">
                <div class="box-icon">
                    <i class="livicon icon" data-name="desktop" data-size="55" data-loop="true" data-c="#01bc8c"
                       data-hc="#01bc8c"></i>
                </div>
                <div class="info">
                    <h3 class="success text-center">Find a Fellowship</h3>
                    <p>Here you will find a home that will walk with you as you grow in your walk of faith and the saving grace of our Lord Jesus Christ. Hebrews 10:25 says "Not forsaking the assembling of ourselves together, as the manner of some is; but exhorting one another: and so much the more , as ye see the day approaching"</p>
                    <div class="text-right primary"><a href="#">Read more</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Responsive Section End -->
        <!-- Easy to Use Section Start -->
        <div class="col-sm-6 col-md-6  col-lg-3 col-12 wow bounceInDown" data-wow-duration="3s" data-wow-delay="0.4s">
            <!-- Box Start -->
            <div class="box">
                <div class="box-icon box-icon1">
                    <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#418bca"
                       data-hc="#418bca"></i>
                </div>
                <div class="info">
                    <h3 class="primary text-center">Sound Bible Teaching</h3>
                    <p>Here you will find a home that will walk with you as you grow in your walk of faith and the saving grace of our Lord Jesus Christ.</p>
                    <div class="text-right primary"><a href="#">Read more</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Easy to Use Section End -->
        <!-- Clean Design Section Start -->
        <div class="col-sm-6 col-md-6  col-lg-3 col-12 wow bounceInUp" data-wow-duration="3s" data-wow-delay="0.8s">
            <div class="box">
                <div class="box-icon box-icon2">
                    <i class="livicon icon1" data-name="doc-portrait" data-size="55" data-loop="true" data-c="#f89a14"
                       data-hc="#f89a14"></i>
                </div>
                <div class="info">
                    <h3 class="warning text-center">Have a teenager/Kid?</h3>
                    <p>Here they will find a home where they are fed all-round in their lives to become great men and women doing exploits for the Kingdom.</p>
                    <div class="text-right primary"><a href="#">Read more</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- //Clean Design Section End -->
        <!-- 20+ Page Section Start -->
        <div class="col-sm-6 col-md-6 col-lg-3 col-12  wow bounceInRight" data-wow-duration="5s" data-wow-delay="1.2s">
            <div class="box">
                <div class="box-icon box-icon3">
                    <i class="livicon icon1" data-name="folder-open" data-size="55" data-loop="true" data-c="#FFD43C"
                       data-hc="#FFD43C"></i>
                </div>
                <div class="info">
                    <h3 class="yellow text-center">God to God Worship</h3>
                    <p>Our services/fellowships are dominated by spirit led worship. The word of God in John 4:23 "But the hour is coming, and now is, 
                       when the true worshippers will worship the Father in spirit and truth; for the father is seeking such to worship Him" </p>
                    <div class="text-right primary"><a href="#">Read more</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- //20+ Page Section End -->
    </div>
    <!-- //Services Section End -->
</div>
<!-- Layout Section Start -->
<section class="feature-main">
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-sm-8 col-12 col-lg-9 wow zoomIn" data-wow-duration="2s">
                <div class="layout-image">
                    <img src="{{ asset('assets/images/layout3.jpg') }}" alt="layout" class="img-fluid"/>
                </div>
            </div>
            <div class="col-md-4 col-lg-3 col-sm-4 col-12 wow lightSpeedIn" data-wow-duration="2s">
                <ul class="list-unstyled  text-right layout-styl">
                    <li>
                        <i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Love
                    </li>
                    <li><i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Pure Worship
                    </li>
                    <li><i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Intercession
                    </li>
                    <li><i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Evangelism
                    </li>
                    <li><i class="livicon" data-name="checked-on" data-size="20" data-loop="true" data-c="#01bc8c"
                           data-hc="#01bc8c"></i> Five-fold ministry
                    </li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- //Layout Section Start -->
<!-- Accordions Section End -->
<div class="container">
    <div class="row">
        <!-- Accordions Start -->
        <div class="text-center col-12 wow flash my-3" data-wow-duration="3s">
            <h3 class="border-success"><span class="heading_border bg-success">Ministries</span></h3>
            <label class=" text-center"> Our Call is to Work out our salvation with fear and trembling (Philippians 2:12) allowing God to use us as His vessels of honor (2 Timothy 2:21) to lead the church into the Holy of holies worship (Ezekiel 44:16).</label>
        </div>
        <!-- Accordions End -->
        <div class="col-md-6 col-sm-12  col-lg-6 col-12 wow slideInLeft" data-wow-duration="1.5s">
            <!-- Tabbable-Panel Start -->
            <div class="tabbable-panel1 index">
                <!-- Tabbablw-line Start -->
                <div class="tabbable-line">
                    <!-- Nav Nav-tabs Start -->
                    <ul class="nav nav-tabs ">
                        <li class="nav-item ">
                            <a href="#tab_default_1" data-toggle="tab" class="nav-link active">
                            Sunday school </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_default_2" data-toggle="tab" class="nav-link">
                            Intercessory </a>
                        </li>
                        <li class="clear_both nav-item">
                            <a href="#tab_default_3" data-toggle="tab" class="nav-link">
                            Worship </a>
                        </li>
                        <li class="nav-item">
                            <a href="#tab_default_4" data-toggle="tab" class="nav-link">
                            Fellowships </a>
                        </li>
                    </ul>
                    <hr class="horizontalline">
                    <!-- //Nav Nav-tabs End -->
                    <!-- Tab-content Start -->
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab_default_1">
                            <div class="media">
                                <div class="media-left tab col-sm-12">
                                    <a href="#">
                                        <img class="media-object img-fluid"
                                             src="{{ asset('assets/images/authors/sundayschool1.jpg') }}" alt="image">
                                    </a>
                                </div>
                            </div>
                            <p>
                                Sunday school ministry caters for the spiritual nourishment of children. It is composed of teachers, teens and the younger children. Proverbs 22 :6 Train up a child in the way he should go: and when he is old, he will not depart from it.
                                Mathew 19:14 "....suffer little children, and forbid them not, to come unto me: for such is the Kingdom of heaven."
                            </p>
                        </div>
                        <div class="tab-pane" id="tab_default_2">
                            <div class="media">
                                <div class="media-left media-middle tab col-sm-12">
                                    <a href="#">
                                        <img class="media-object img-fluid"
                                             src="{{ asset('assets/images/authors/intercessory1.jpg') }}" alt="image">
                                    </a>
                                </div>
                            </div>
                            <p> We are great team that believe in consistently dwelling in God’s presence and seeking His face always.The Bible says in 1st Thessalonians 5:16-17 Rejoice always, pray without ceasing.</p>
                        </div>
                        <div class="tab-pane" id="tab_default_3">
                            <div class="media">
                                <div class="media-left media-middle tab col-sm-12">
                                    <a href="#">
                                        <img class="media-object img-fluid"
                                             src="{{ asset('assets/images/authors/worship1.jpg') }}" alt="image">
                                    </a>
                                </div>
                            </div>
                            <p>
                                Music is a powerful tool of ministry. Its one of the major part of a growing and vigilant congregation. When done under the leading of the Holy Spirit,
                                and with revelation the results are glorious. Extensive salvation, wholesome healing, deliverance are all achievable in both individual as well as corporate worship.
                            </p>
                        </div>
                        <div class="tab-pane" id="tab_default_4">
                            <div class="media">
                                <div class="media-left media-middle tab col-sm-12">
                                    <a href="#">
                                        <img class="media-object img-fluid"
                                             src="{{ asset('assets/images/authors/fellowship1.jpg') }}" alt="image">
                                    </a>
                                </div>
                            </div>
                            <p>
                               Many people, including Christians, lack close friends. When people get together in a small group, home fellowship group, or cell group, close friendships form and often remain long after the group ends.
                            </p>
                        </div>
                    </div>
                    <!-- Tab-content End -->
                </div>
                <!-- //Tabbablw-line End -->
            </div>
            <!-- Tabbable_panel End -->
        </div>
        <div class="col-md-6 col-sm-12 col-12 col-lg-6 wow slideInRight" data-wow-duration="3s">
            <div id="accordion">
                <div class="card card_collapse">
                    <div class="card-header header_card" id="headingOne">
                        <h5 class="mb-0">
                            <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne"
                                    aria-expanded="true" aria-controls="collapseOne">
                                <i class="fa fa-minus success"></i>
                                <span class="success">Upperroom Mission</span>
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            <p> The objective of Chrisco UpperRoom is to;
                               To preach the full gospel of Jesus Christ to mankind. This includes providing for the total needs of man including physical needs, spiritual and academic needs, domestic and material needs.
                               Training and equipping men and women in the five-fold ministries, ordaining and sending forth ministers and workers to different parts of the country, sending missionaries to other parts of the world.
                               Establishing training institutions i.e. vocational schools, bible training schools, academic institutions and correspondence schools.
                               Planting churches within and outside Kenya.
                               To reach out through the mass media through CHRISCO owned television station.
                               To raise up an army of intercessors who’ll stand up for Africa and the rest of the world.
                               To reach out to the young generation.
                               To help the less fortunate in the society through building children’s homes, hospitals and building homes for the aged.
                               To own money generating projects those are going to finance the work of God. 
                            </p>
                        </div>
                    </div>
                </div>
                <div class="card card_collapse">
                    <div class="card-header header_card" id="headingTwo">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseTwo"
                                    aria-expanded="false" aria-controls="collapseTwo">
                                <i class="fa fa-plus success"></i>
                                <span class="success">Upperroom Vision</span>
                            </button>
                        </h5>
                    </div>
                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                        <div class="card-body">
                        <p>
                            The vision of CHRISCO church is entailed in pioneering apostolic work in Kenya and preparing the brethren and the nation to be the spring board for the end time revival.
                            The vision includes:Teaching about Intercession in the New Testament pattern.Teaching about worship in the New Testament pattern.
                            Raising able ministers who will reach out to the world with the gospel.Pioneering a move of God that’s going to sweep the rest of the world.Sound Doctrine Teaching - We conduct training sessions to missioners to equip them with godly doctrine.
                        </p>
                        </div>
                    </div>
                </div>
                <div class="card card_collapse">
                    <div class="card-header header_card" id="headingThree">
                        <h5 class="mb-0">
                            <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapseThree"
                                    aria-expanded="false" aria-controls="collapseThree">
                                <i class="fa fa-plus success"></i>
                                <span class="success">What we believe in</span>
                            </button>
                        </h5>
                    </div>
                    <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                        <div class="card-body">
                            <p> The vision of CHRISCO church is entailed in pioneering apostolic work in Kenya and preparing the brethren and the nation to be the spring board for the end time revival.
                                The vision includes:Teaching about Intercession in the New Testament pattern.Teaching about worship in the New Testament pattern.Raising able ministers who will reach out to the world with the gospel.
                                Pioneering a move of God that’s going to sweep the rest of the world. Sound Doctrine Teaching - We conduct training sessions to missioners to equip them with godly doctrine.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            <!--Panel-group Start -->
            <!--<div class="panel-group" id="accordion">-->
            <!--&lt;!&ndash; Panel Panel-default Start &ndash;&gt;-->
            <!--<div class="panel panel-default">-->
            <!--&lt;!&ndash; Panel-heading Start &ndash;&gt;-->
            <!--<div class="panel-heading text_bg">-->
            <!--<h4 class="panel-title">-->
            <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">-->
            <!--&lt;!&ndash;<span class=" glyphicon glyphicon-minus success"></span>&ndash;&gt;-->
            <!--<i class="fa fa-plus success"></i>-->
            <!--<span class="success">Why Choose Us</span></a>-->
            <!--</h4>-->
            <!--</div>-->
            <!--&lt;!&ndash; //Panel-heading End &ndash;&gt;-->
            <!--&lt;!&ndash; Collapseone Start &ndash;&gt;-->
            <!--<div id="collapseOne" class="panel-collapse collapse in">-->
            <!--<div class="panel-body">-->
            <!--<p>In 1972 a crack commando unit was sent to prison by a military court for a crime they didn't commit. These men promptly escaped from a maximum security stockade to the Los Angeles underground. Believe it or not I'm walking on air. I never thought I could feel so free. Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. Come and knock on our door. We've been waiting for you. Where the kisses are hers and hers and his. Three's company too. Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. Here's the story of a man named Brady who was busy with three boys of his own. One two three four five six seven eight Sclemeel schlemazel hasenfeffer incorporated? Till the one day when the lady met this fellow and they knew it was much more than a hunch. Baby if you've ever wondered.-->
            <!--</p>-->
            <!--</div>-->
            <!--</div>-->
            <!--&lt;!&ndash; Collapseone End &ndash;&gt;-->
            <!--</div>-->
            <!--&lt;!&ndash; //Panel Panel-default End &ndash;&gt;-->
            <!--<div class="panel panel-default">-->
            <!--<div class="panel-heading text_bg">-->
            <!--<h4 class="panel-title">-->
            <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">-->
            <!--<i class="fa fa-plus success"></i>-->
            <!--<span class="success">Why Choose Us</span>-->
            <!--</a>-->
            <!--</h4>-->
            <!--</div>-->
            <!--<div id="collapseTwo" class="panel-collapse collapse">-->
            <!--<div class="panel-body">-->
            <!--<p>-->
            <!--In 1972 a crack commando unit was sent to prison by a military court for a crime they didn't commit. These men promptly escaped from a maximum security stockade to the Los Angeles underground. Believe it or not I'm walking on air. I never thought I could feel so free. Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. Come and knock on our door. We've been waiting for you. Where the kisses are hers and hers and his. Three's company too. Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. Here's the story of a man named Brady who was busy with three boys of his own. One two three four five six seven eight Sclemeel schlemazel hasenfeffer incorporated? Till the one day when the lady met this fellow and they knew it was much more than a hunch. Baby if you've ever wondered.-->
            <!--</p>-->
            <!--</div>-->
            <!--</div>-->
            <!--</div>-->
            <!--<div class="panel panel-default">-->
            <!--<div class="panel-heading text_bg">-->
            <!--<h4 class="panel-title">-->
            <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">-->
            <!--<i class="fa fa-plus success"></i>-->
            <!--<span class="success">Why Choose Us</span></a>-->
            <!--</h4>-->
            <!--</div>-->
            <!--<div id="collapseThree" class="panel-collapse collapse">-->
            <!--<div class="panel-body">-->
            <!--<p>-->
            <!--In 1972 a crack commando unit was sent to prison by a military court for a crime they didn't commit. These men promptly escaped from a maximum security stockade to the Los Angeles underground. Believe it or not I'm walking on air. I never thought I could feel so free. Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. Come and knock on our door. We've been waiting for you. Where the kisses are hers and hers and his. Three's company too. Flying away on a wing and a prayer. Who could it be? Believe it or not its just me. Here's the story of a man named Brady who was busy with three boys of his own. One two three four five six seven eight Sclemeel schlemazel hasenfeffer incorporated? Till the one day when the lady met this fellow and they knew it was much more than a hunch. Baby if you've ever wondered.-->
            <!--</p>-->
            <!--</div>-->
            <!--</div>-->
            <!--</div>-->
            <!--</div>-->
            <!--&lt;!&ndash; //Panle-group End &ndash;&gt;-->
            <!--</div>-->
        </div>
    </div>
</div>
<!-- //Accordions Section End -->
<!-- Our Team Start -->
<div class="container">
    <div class="row text-center">
        <div class="col-12 my-3">
            <h3 class=" border-danger"><span class="heading_border bg-danger">Our Team</span></h3></div>
    </div>
    <div class="row text-center">
        <div class="col-md-6 col-sm-6 col-12 col-lg-3 profile wow fadeInLeft" data-wow-duration="3s"
             data-wow-delay="0.5s">
            <div class="thumbnail bg-white">
                <img src="{{ asset('assets/images/parents1.jpg') }}" alt="team-image" class="img-fluid">
                <div class="caption">
                    <b>Pst Kato & Pst Joan</b>
                    <p class="text-center"> Founding Pastor and Wife</p>
                    <div class="divide">
                        <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider">
                            <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795"
                               data-hc="#3a5795"></i>
                        </a>
                        <a href="https://twitter.com/CUpperRoom">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee"
                               data-hc="#55acee"></i>
                        </a>
                        <a href="https://plus.google.com/112218850217091238784">
                            <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32"
                               data-hc="#d73d32"></i>
                        </a>
                        <a href="#">
                            <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd"
                               data-hc="#1b86bd"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3 col-12 profile wow fadeInUp" data-wow-duration="3s"
             data-wow-delay="0.5s">
            <div class="thumbnail bg-white">
                <img src="{{ asset('assets/images/eldership1.jpg') }}" alt="team-image">
                <div class="caption">
                    <b>Church leadership</b>
                    <p class="text-center"> Elders, deacons & deaconnesses </p>
                    <div class="divide">
                        <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider">
                            <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795"
                               data-hc="#3a5795"></i>
                        </a>
                        <a href="https://twitter.com/CUpperRoom">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee"
                               data-hc="#55acee"></i>
                        </a>
                        <a href="https://plus.google.com/112218850217091238784">
                            <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32"
                               data-hc="#d73d32"></i>
                        </a>
                        <a href="#">
                            <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd"
                               data-hc="#1b86bd"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3 col-12 profile wow fadeInDown" data-wow-duration="3s"
             data-wow-delay="0.5s">
            <div class="thumbnail bg-white">
                <img src="{{ asset('assets/images/aerial1.jpg') }}" alt="team-image" class="img-fluid">
                <div class="caption">
                    <b>Worship team</b>
                    <p class="text-center"> Instrumentalists & Singers </p>
                    <div class="divide">
                        <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider">
                            <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795"
                               data-hc="#3a5795"></i>
                        </a>
                        <a href="https://twitter.com/CUpperRoom">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee"
                               data-hc="#55acee"></i>
                        </a>
                        <a href="https://plus.google.com/112218850217091238784">
                            <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32"
                               data-hc="#d73d32"></i>
                        </a>
                        <a href="#">
                            <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd"
                               data-hc="#1b86bd"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-sm-6 col-lg-3 col-12 profile wow fadeInRight" data-wow-duration="3s"
             data-wow-delay="0.5s">
            <div class="thumbnail bg-white">
                <img src="{{ asset('assets/images/church1.jpg') }}" alt="team-image">
                <div class="caption">
                    <b>Mama Das with Upperroom fellowship</b>
                    <p class="text-center"> Chrisco Upperroom </p>
                    <div class="divide">
                        <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider">
                            <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795"
                               data-hc="#3a5795"></i>
                        </a>
                        <a href="https://twitter.com/CUpperRoom">
                            <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee"
                               data-hc="#55acee"></i>
                        </a>
                        <a href="https://plus.google.com/112218850217091238784">
                            <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32"
                               data-hc="#d73d32"></i>
                        </a>
                        <a href="#">
                            <i class="livicon" data-name="linkedin" data-size="22" data-loop="true" data-c="#1b86bd"
                               data-hc="#1b86bd"></i>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- //Our Team End -->
    <!-- What we are section Start -->
    <div class="row">
        <!-- What we are Start -->
        <div class="col-md-6 col-sm-6 col-lg-6 col-12 wow zoomInLeft" data-wow-duration="3s">
            <div class="text-left">
                <div class="mt-2">
                    <h4 class="border-warning"><span class="heading_border bg-warning">Chrisco upperroom Ministry</span></h4>
                </div>
            </div>
            <img src="{{ asset('assets/images/chrisco1.jpg') }}" alt="image_12" class="img-fluid">
            <p>
                CHRISCO Work founders Apostle Harry Das and Mama Das saw the great need and commissioned formation of Upper Room Church in Nairobi, under leadership and pastoral care of Pastor Kato and Joan Lafont, to provide a home where the young people could grow, breed, express themselves and eventually mature.This is where talents and gifts have been identified, nurtured, guided, exposed to the right environment to flourish and unfold. Many great men and women in the nation and outside Africa testify to the home they found under care of Pastors Kato and Joan Lafont. Under their watch there is balancing, edification, exaltation unto holiness and uprightness in serving God and working out our salvation with diligence and in fear of God.
                Ministering to the Spirit and Body needs of the kingdom of God. A man with a fatherly heart towards young people. Preaching the gospel of love, purity, holiness and righteousness through Jesus Christ. Equipping you to be an able minister ready to serve God in the capacity of your calling
            </p>
            <p class="text-right primary my-2"><a href="#">Read more</a>
            </p>
        </div>
        <!-- //What we are End -->
        <!-- About Us Start -->
        <div class="col-md-6 col-sm-6 col-lg-6 col-12 wow zoomInRight" data-wow-duration="3s">
            <div class="text-left">
                <div class="mt-2">
                    <h4 class="border-success"><span class="heading_border bg-success">Fly-M Ministry</span></h4>
                </div>
            </div>
            <img src="{{ asset('assets/images/flym1.jpg') }}" alt="image_11" class="img-fluid">
            <p>
                Today, with one of the fastest growing youth ministry in Kenya, Fly-m has grown tremendously under Pastor Kato and Joan Lafont to become the platform of evangelism to Schools - from Primary Schools all the way to institutions of higher learning. Pastor Lafont Kato and the leaders serving under him oversee a massive congregation and this dynamic ministry under him has and is transforming youths especially through missions and outreach programs.
            </p>
            <p class="text-right primary my-2"><a href="#">Read more</a>
        </div>
        <!-- //About Us End -->
    </div>
    <!-- //What we are section End -->

    <!-- Our Skills Start -->

    <div class=" col-12 text-center my-3 marbtm10">
        <h3 class="border-danger"><span class="heading_border bg-danger">Our Commitments</span></h3>
    </div>
</div>
    <!--</div>-->
    <div class="sliders">
        <!-- Our skill Section start -->
        <div class="container">
            <div class="row">
                <div class="col-md-3 col-sm-6 col-lg-3 col-12 text-center wow zoomIn" data-wow-duration="3.5s">
                    <div class="mx-auto">
                        <div id="myStat3" class="mx-auto" data-startdegree="0" data-dimension="150" data-text="95%"
                             data-width="4" data-fontsize="28" data-percent="95" data-fgcolor="#3abec0"
                             data-bgcolor="#eee"></div>
                        <h4 class="success"><strong>Evangelism</strong></h4>
                    </div>
                    <p class="my-3">We as a fellowship are commited to the great commission given to us by the Lord to reach out to the lost soul in dire need of.</p>
                </div>
                <div class="col-md-3 col-sm-6 col-lg-3 col-12 text-center wow zoomIn" data-wow-duration="3s"
                     data-wow-delay="0.8s">
                    <div class="mx-auto">
                        <div id="myStat4" class="mx-auto" data-startdegree="0" data-dimension="150" data-text="90%"
                             data-width="4" data-fontsize="28" data-percent="90" data-fgcolor="#3abec0"
                             data-bgcolor="#eee"></div>
                        <h4 class="success"><strong>Intercession</strong></h4>
                    </div>
                    <p class="my-3">Our aim is to travail for the revival, pray for the body of Christ and the Nation at large</p>
                </div>
                <div class="col-md-3 col-sm-6 col-lg-3 col-12 text-center wow zoomIn" data-wow-duration="3s"
                     data-wow-delay="1.2s">
                    <div class="mx-auto">
                        <div id="myStat5" class="mx-auto" data-startdegree="0" data-dimension="150" data-text="100%"
                             data-width="4" data-fontsize="28" data-percent="100" data-fgcolor="#3abec0"
                             data-bgcolor="#eee"></div>
                        <h4 class="success"><strong>Love</strong></h4>
                    </div>
                    <p class="my-3">Love is our strongest pillar and essential to the fellowship</p>
                </div>
                <div class="col-md-3 col-sm-6 col-lg-3 col-12 text-center wow zoomIn" data-wow-duration="3s"
                     data-wow-delay="1.8s">
                    <div class="mx-auto">
                        <div id="myStat6" class="mx-auto" data-startdegree="0" data-dimension="150" data-text="100%"
                             data-width="4" data-fontsize="28" data-percent="100" data-fgcolor="#3abec0"
                             data-bgcolor="#eee"></div>
                        <h4 class="success"><strong>Worship</strong></h4>
                    </div>
                    <p class="my-3">In the fellowship we  give a God to God kind of worship.</p>
                </div>
            </div>
            <!-- Our skills Section End -->
        </div>
        <!-- //Our Skills End -->
    </div>

<!-- //Container End -->
@stop
{{-- footer scripts --}}
@section('footer_scripts')
<!-- page level js starts-->
<script type="text/javascript" src="{{ asset('assets/js/frontend/jquery.circliful.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/wow/js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/frontend/carousel.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/frontend/index.js') }}"></script>
<!--page level js ends-->
@stop


