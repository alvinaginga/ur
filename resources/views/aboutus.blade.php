@extends('layouts/default')

{{-- Page title --}}
@section('title')
About us
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/aboutus.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/animate/animate.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/owl_carousel/css/owl.theme.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/devicon/devicon.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/devicon/devicon-colors.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">
            <ol class="breadcrumb">
                <li>
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-sm-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">About Us</a>
                </li>
            </ol>
            <div class="float-right breadcrum_adjust">
                <i class="livicon icon3" data-name="users" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> About Us
            </div>
        </div>
    </div>

        </div>
    </div>
    @stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container">
        <!-- Slider Section Start -->
        <div class="row my-3">
            <!-- Left Heading Section Start -->
            <div class="col-md-7 col-sm-12  col-md-12 col-lg-8 wow bounceInLeft" data-wow-duration="5s">
                <h2><label>Welcome to Chrisco Upperroom</label></h2>
                <p>
                    Here at Chrisco Upper-Room Church, we’re passionate about people of all walks of life finding a place to call home in our church. We inspire and equip all generations to - connect, grow, and live - enjoying God's love and saving grace!
                </p>
                <p>
                    The Church recognizes that the church is built upon the foundation of Apostles and Prophets and that God sends his Apostles and Prophets to pioneer every new thing he desires to do on earth. God send Apostle Das to pioneer this apostolic move that is going to sweep the entire world in preparation for Christ’s return.
                </p>
                <p>
                    We believe that God wants to have a personal and dynamic relationship with us all. Getting together for Sunday fellowship is only a small part of how we grow spiritually.
                </p>
            </div>
            <!-- //Left Heaing Section End -->
            <!-- Slider Start -->
            <div class="col-md-12 col-sm-12  col-lg-4 slider wow fadeInRightBig" data-wow-duration="5s">
                <div id="owl-demo" class="owl-carousel owl-theme">
                    <div class="item"><img src="{{ asset('assets/images/au1.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au2.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au3.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au4.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au5.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au6.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au7.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au8.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au9.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au10.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au11.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au12.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                    <div class="item"><img src="{{ asset('assets/images/au13.jpg') }}" alt="slider-image" class="img-fluid">
                    </div>
                </div>
            </div>
            <!-- //Slider End -->
        </div>
        <!-- //Slider Section End -->
        <!-- Services Section Start -->
        <div class="text-center">
            <h3 class="border-success"><span class="heading_border bg-success" >What to expect</span></h3>
        </div>
        <div class="row">
            <!-- left Section Start -->
            <div class="col-md-12 col-sm-12 col-lg-6 col-12">
                <div class="row">
                <!-- Responsive Section Start -->
                <div class="col-sm-6 col-md-12 col-lg-6 col-12 wow zoomIn" data-wow-duration="3s">
                    <div class="box">
                        <div class="box-icon">
                            <i class="livicon icon1" data-name="desktop" data-size="55" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                        </div>
                        <div class="info">
                            <h3 class="success text-center mt-3">Find a fellowship</h3>
                            <p>Here you will find a home that will walk with you as you grow in your walk of faith and the saving grace of our Lord Jesus Christ. Hebrews 10:25 says "Not forsaking the assembling of ourselves together, as the manner of some is; but exhorting one another: and so much the more , as ye see the day approaching"</p>
                            <div class="text-right primary"><a href="#">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Responsive Section End -->
                <!-- Easy to Use Section Start -->
                <div class="col-sm-6 col-md-12 col-lg-6 col-12 wow zoomIn" data-wow-duration="3s">
                    <div class="box">
                        <div class="box-icon box-icon1">
                            <i class="livicon icon1" data-name="gears" data-size="55" data-loop="true" data-c="#418bca" data-hc="#418bca"></i>
                        </div>
                        <div class="info">
                            <h3 class="primary text-center mt-3">Sound Bible Teaching</h3>
                            <p>Here you will find a home that will walk with you as you grow in your walk of faith and the saving grace of our Lord Jesus Christ</p>
                            <div class="text-right primary"><a href="#">Read more</a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- //Easy to use Section End -->
            </div>
            </div>
            <!-- Left Section End -->
            <div class="col-md-12 col-sm-12 col-lg-6 col-12 wow bounceInRight" data-wow-duration="3s">
                <!-- Pnael group section Start -->
                <div class="margin-t30 d-none d-md-block"></div>
                <div  id="accordion">
                    <div class="card card_collapse">
                        <div class="card-header header_card" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-minus success"></i>
                                    <span class="success">Upperroom Mission</span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="media">
                                <div class="media-top">
                                    <a href="#">
                                    <!--    <i class="devicon-php-plain colored font100" ></i> -->
                                    </a>
                                </div>
                                <div class="media-body">
                                    <p class="media-heading">The objective of Chrisco UpperRoom is to; To preach the full gospel of Jesus Christ to mankind. This includes providing for the total needs of man including physical needs, spiritual and academic needs, domestic and material needs. Training and equipping men and women in the five-fold ministries, ordaining and sending forth ministers and workers to different parts of the country, sending missionaries to other parts of the world. Establishing training institutions i.e. vocational schools, bible training schools, academic institutions and correspondence schools. Planting churches within and outside Kenya. To reach out through the mass media through CHRISCO owned television station. To raise up an army of intercessors who’ll stand up for Africa and the rest of the world. To reach out to the young generation. To help the less fortunate in the society through building children’s homes, hospitals and building homes for the aged. To own money generating projects those are going to finance the work of God.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Php Section End -->
                    <!-- Html Section Start -->
                    <div class="card card_collapse">
                        <div class="card-header header_card" id="headingtwo">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapsetwo" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-plus success"></i>
                                    <span class="success">Upperroom Vision</span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapsetwo" class="collapse" aria-labelledby="headingtwo" data-parent="#accordion">
                            <div class="card-body">
                                <div class="media">
                                <div class="media-left media-top">
                                <a href="#">
                                <!-- <i class="devicon-html5-plain colored font100"></i> -->
                                </a>
                                </div>
                                <div class="media-body">
                                <p class="media-heading">The vision of CHRISCO church is entailed in pioneering apostolic work in Kenya and preparing the brethren and the nation to be the spring board for the end time revival. The vision includes:Teaching about Intercession in the New Testament pattern.Teaching about worship in the New Testament pattern. Raising able ministers who will reach out to the world with the gospel.Pioneering a move of God that’s going to sweep the rest of the world.Sound Doctrine Teaching - We conduct training sessions to missioners to equip them with godly doctrine.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card card_collapse">
                        <div class="card-header header_card" id="headingthree">
                            <h5 class="mb-0">
                                <button class="btn btn-link" data-toggle="collapse" data-target="#collapsethree" aria-expanded="true" aria-controls="collapseOne">
                                    <i class="fa fa-plus success"></i>
                                    <span class="success">What we believe in</span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapsethree" class="collapse" aria-labelledby="headingthree" data-parent="#accordion">
                            <div class="card-body">
                            <div class="media">
                            <div class="media-left media-top">
                            <a href="#">
                           <!-- <i class="devicon-jquery-plain colored font100" ></i> -->
                            </a>
                            </div>
                            <div class="media-body">
                            <p class="media-heading">The vision of CHRISCO church is entailed in pioneering apostolic work in Kenya and preparing the brethren and the nation to be the spring board for the end time revival. The vision includes:Teaching about Intercession in the New Testament pattern.Teaching about worship in the New Testament pattern.Raising able ministers who will reach out to the world with the gospel. Pioneering a move of God that’s going to sweep the rest of the world. Sound Doctrine Teaching - We conduct training sessions to missioners to equip them with godly doctrine.</p>
                            </div>
                            </div>
                            </div>
                        </div>
                    </div>
                    <!-- //Html Section End -->
                    <!-- Jquery Section Start -->
                    <!--<div class="card card_collapse">-->
                        <!--<div class="card-heading text_bg">-->
                            <!--<h4 class="card-title">-->
                                <!--<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">-->
                                    <!--<span class="fa fa-plus success"></span>-->
                                <!--<span class="success">JQUERY</span></a>-->
                            <!--</h4>-->
                        <!--</div>-->
                        <!--<div id="collapseThree" class="panel-collapse collapse">-->
                            <!--<div class="card-body">-->
                                <!--<div class="media">-->
                                    <!--<div class="media-left media-top">-->
                                        <!--<a href="#">-->
                                            <!--<i class="devicon-jquery-plain colored font100" ></i>-->
                                        <!--</a>-->
                                    <!--</div>-->
                                    <!--<div class="media-body">-->
                                        <!--<p class="media-heading">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged.</p>-->
                                    <!--</div>-->
                                <!--</div>-->
                            <!--</div>-->
                        <!--</div>-->
                    <!--</div>-->
                    <!-- //Jquery Section End -->
                </div>
                <!-- //Panel group Section End -->
            </div>
        </div>
        <!-- // Services Section End -->
        <!-- Our Team Section Start -->
        <div class="text-center my-3">
            <h3 class="border-danger"><span class="heading_border bg-danger" >Our Team</span></h3>
        </div>
        <div class="row">
            <!-- Our Team Heading Start -->
            <!-- //Our Team Heading End -->
            <!-- Image1 Section Start -->
            <div class="col-md-6 col-sm-12 col-12  col-lg-3 profile wow zoomIn" data-wow-duration="3.5s">
                <div class="thumbnail bg-white  text-center">
                    <img src="{{ asset('assets/images/lkato.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Pst Kato Lafont</b>
                        <br /> Senior Pastor &amp; Founder
                        <br />
                        <div class="divide">
                            <a href="https://www.facebook.com/kato.c.lafont?fref=ts" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image1 Section End -->
            <!-- Image2 Section Start -->
            <div class="col-md-6 col-sm-12 col-12 col-lg-3 profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white text-center">
                    <img src="{{ asset('assets/images/ljoan.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Pastor Joan Lafont</b>
                        <br /> Pastor &amp;Co-founder
                        <br />
                        <div class="divide">
                            <a href="https://www.facebook.com/joan.lafont.7?fref=ts" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image2 Section End -->
            <!-- Image3 Section Start -->
            <div class="col-md-6 col-sm-12 col-12 col-lg-3  profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white  text-center">
                    <img src="{{ asset('assets/images/leadership1.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Elder Akaranga &amp; Pst Maunda</b>
                        <br /> Elder &amp; Pastor in Training
                        <br />
                        <div class="divide">
                            <a href="#" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="#"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Image3 Section End -->
            <!-- Image4 Section Star -->
            <div class="col-md-6 col-sm-5 col-12  col-lg-3 profile wow zoomIn" data-wow-duration="3s" data-wow-delay="0.8s">
                <div class="thumbnail bg-white text-center">
                    <img src="{{ asset('assets/images/lmawira.jpg') }}" alt="team-image" class="img-fluid">
                    <div class="caption">
                        <b>Elder Mawira</b>
                        <br /> Elder &amp; Church treasurer
                        <br />
                        <div class="divide">
                            <a href="https://www.facebook.com/chriscoupperroomfellowship/?ref=br_rs" class="divider"> <i class="livicon" data-name="facebook" data-size="22" data-loop="true" data-c="#3a5795" data-hc="#3a5795"></i>
                            </a>
                            <a href="https://twitter.com/CUpperRoom"> <i class="livicon" data-name="twitter" data-size="22" data-loop="true" data-c="#55acee" data-hc="#55acee"></i>
                            </a>
                            <a href="https://plus.google.com/112218850217091238784"> <i class="livicon" data-name="google-plus" data-size="22" data-loop="true" data-c="#d73d32" data-hc="#d73d32"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- //Image4 Section End -->
        </div>
        <!-- //Our Team Section End -->
    </div>
    
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('assets/vendors/owl_carousel/js/owl.carousel.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/wow/js/wow.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/carousel.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/aboutus.js') }}"></script>
    <!--page level js ends-->
@stop
