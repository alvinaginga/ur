@extends('layouts/default')

{{-- Page title --}}
@section('title')
Worship | Chrisco Upperroom
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/cart.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/tabbular.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/bootstrap-rating/bootstrap-rating.css') }}">
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">


            <ol class="breadcrumb">
                <li  class="breadcrumb-item">
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-sm-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">Worship</a>
                </li>
            </ol>
            <div class="float-right breadcrum_adjust">
                <i class="livicon icon3" data-name="edit" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Worship
            </div>
        </div>
    </div>
        </div>
    </div>
@stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container my-3">
        <!--item view start-->
        <div class="row">
            <!--<div class="mart10">-->
                <!--product view-->
                <div class="col-md-4 col-lg-4 col-sm-6 col-12">
                    <div class="row">
                        <div class="product_wrapper">
                            <img id="zoom_09" src="{{ asset('assets/images/wt/wts1.jpg') }}" data-zoom-image="{{ asset('assets/images/wt/wtb1.jpg') }}" class="img-fluid" />
                        </div>
                    </div>
                    <div class="row">
                        <!--individual products in product view-->
                        <div id="gal1">
                            <a href="#" data-image="{{ asset('assets/images/wt/wts1.jpg') }}" data-zoom-image="{{ asset('assets/images/wt/wtb1.jpg') }}">
                                <img id="img_01" src="{{ asset('assets/images/wt/wts1.jpg') }}" class="img-fluid" />
                            </a>
                            <a href="#" data-image="{{ asset('assets/images/wt/wts2.jpg') }}" data-zoom-image="{{ asset('assets/images/wt/wtb2.jpg') }}">
                                <img id="img_01" src="{{ asset('assets/images/wt/wts2.jpg') }}" class="img-fluid" />
                            </a>
                            <a href="#" data-image="{{ asset('assets/images/wt/wts3.jpg') }}" data-zoom-image="{{ asset('assets/images/wt/wtb3.jpg') }}">
                                <img id="img_01" src="{{ asset('assets/images/wt/wts3.jpg') }}" class="img-fluid" />
                            </a>
                            <a href="#" data-image="{{ asset('assets/images/wt/wts4.jpg') }}" data-zoom-image="{{ asset('assets/images/wt/wtb4.jpg') }}">
                                <img id="img_01" src="{{ asset('assets/images/wt/wts4.jpg') }}" class="img-fluid" />
                            </a>
                        </div>
                    </div>
                </div>
                <!--individual product description-->
                <div class="col-md-8 col-sm-6 col-lg-8 col-12 ml-auto">
                    <h2 class="text-primary">Chrisco Upperroom worship team </h2>
                    <p>Real worship is more than singing praises; it is the act of giving away our hearts. Worship is attributing ultimate value to something; it thinks, “If I had that I’d be happy;” it is a deep belief of the heart that says, “That is all I need.”
                       Worship is what we most deeply value. It’s not just the times we set aside to sing praise songs. We are constantly worshipping. Moment-by-moment, we live for something. “Where our treasure is, there will our hearts and minds be also.”
                       Instead of singing, I meditated on the Psalms; in place of rhythm, I read the gospels. Worship is not a feeling as much as the place of our deepest trust. Worship is a heart-rest on God.
                       Our Call is to Work out our salvation with fear and trembling (Philippians 2:12) allowing God to use us as His vessels of honor (2 Timothy 2:21) to lead the church into the Holy of holies worship (Ezekiel 44:16)
                    </p>        
                </div>
            </div>
        <!--</div>-->
        <!--item view end-->
        <!--item desciption start-->
        <div class="row">
            <div class="col-sm-12">
                <!-- Tabbable-Panel Start -->
                <div class="tabbable-panel">
                    <!-- Tabbablw-line Start -->
                    <div class="tabbable-line">
                        <!-- Nav Nav-tabs Start -->
                        <ul class="nav nav-tabs ">
                            <li class="nav-item ">
                                <a href="#tab_default_1" data-toggle="tab" class="nav-link active">
                                Description </a>
                            </li>
                        </ul>
                        <hr class="horizontalline">
                        <!-- //Nav Nav-tabs End -->
                        <!-- Tab-content Start -->
                        <div class="tab-content">
                                <div class="tab-pane active show fade" id="tab_default_1">
                                        <p> Music is a powerful tool of ministry. Its one of the major part of a growing and vigilant congregation. When done under the leading of the Holy Spirit, and with revelation the results are glorious. Extensive salvation, wholesome healing, deliverance are all achievable in both individual as well as corporate worship.
                                        The Chrisco Upper room Worship Team is the team mandated to lead the Church though sessions of Worship in all services including outdoor Missions. Our Call is to Work out our salvation with fear and trembling (Philippians 2:12) allowing God to use us as His vessels of honor (2 Timothy 2:21) to lead the church into the Holy of holies worship (Ezekiel 44:16) We edify the body of Christ through psalms, music and song.
                                        </p>
                                        <ul>
                                            <li><i class="livicon" data-name="check" data-size="18" data-loop="true" data-c="#555555" data-hc="#555555"></i> Worship is all about reflecting the worth or value of God</li>
                                            <li><i class="livicon" data-name="check" data-size="18" data-loop="true" data-c="#555555" data-hc="#555555"></i>Christ is praised in death by being prized above life</li>
                                            <li><i class="livicon" data-name="check" data-size="18" data-loop="true" data-c="#555555" data-hc="#555555"></i>Genuine affections for God are an end in themselves.</li>
                                            <li><i class="livicon" data-name="check" data-size="18" data-loop="true" data-c="#555555" data-hc="#555555"></i>The pursuit of joy in God is not optional. It is our highest duty.</li>
                                        </ul>
                                        <p> Our Target is to have 1000 instruments played by skillful, anointed and Spirit-filled brethren. By this the Church can experience another dimension of glory in a level than what was the encounter in 2 Chronicles 5,
                                            whereby as the instrumentalists and singers raised their voices as one, the cloud of God’s glory filled the temple until the Priests could not be able to minister.
                                        </p>
                        
                            </div>
                            <!-- Tab-content End -->
                        </div>
                        <!-- //Tabbable-line End -->
                    </div>
                    <!-- Tabbable_panel End -->
                </div>
            </div>
        </div>
        <!--item desciption end-->
        <!--recently view item-->
        <h2 class="text-primary"> Worship team Gallery</h2>
        <div class="divider"></div>
        <div class="row">
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta1.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta2.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta3.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta5.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta6.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta7.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta8.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta9.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta10.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta11.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta12.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta13.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta14.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta15.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta16.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta17.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta18.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta19.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta20.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta21.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta22.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta23.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta24.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
            <div class="flip-3d">
                <figure>
                    <img src="{{ asset('assets/images/wt/wta25.jpg') }}" alt="worship image" class="img-fluid">
                </figure>
            </div>
        </div>
        <!--recently view item end-->
    </div>
    <!-- //Container Section End -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!--page level js start-->
    <script type="text/javascript" src="{{ asset('assets/js/frontend/elevatezoom.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/vendors/bootstrap-rating/bootstrap-rating.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/cart.js') }}"></script>
    <!--page level js start-->

@stop
