@extends('layouts/default')

{{-- Page title --}}
@section('title')
Portfolio
@parent
@stop

{{-- page level styles --}}
@section('header_styles')
    <!--page level css starts-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/css/frontend/portfolio.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('assets/vendors/fancybox/css/jquery.fancybox.css') }}"
      media="screen"/>
<!-- Add Button helper (this is optional) -->
<link rel="stylesheet" type="text/css"
      href="{{ asset('assets/vendors/fancybox/css/jquery.fancybox-buttons.css') }}"/>
<!-- Add Thumbnail helper (this is optional) -->
<link rel="stylesheet" type="text/css"
      href="{{ asset('assets/vendors/fancybox/css/jquery.fancybox-thumbs.css') }}"/>
    <!--end of page level css-->
@stop

{{-- breadcrumb --}}
@section('top')
    <div class="breadcum">
        <div class="container">
            <div class="row">
                <div class="col-12">
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="{{ route('home') }}"> <i class="livicon icon3 icon4" data-name="home" data-size="18" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i>Dashboard
                    </a>
                </li>
                <li class="d-none d-sm-block">
                    <i class="livicon icon3" data-name="angle-double-right" data-size="18" data-loop="true" data-c="#01bc8c" data-hc="#01bc8c"></i>
                    <a href="#">Multimedia</a>
                </li>
            </ol>
            <div class="float-right breadcrum_adjust">
                <i class="livicon icon3" data-name="briefcase" data-size="20" data-loop="true" data-c="#3d3d3d" data-hc="#3d3d3d"></i> Multimedia
            </div>
        </div>
    </div>
        </div>
    </div>
    @stop


{{-- Page content --}}
@section('content')
    <!-- Container Section Start -->
    <div class="container my-3">
        <!-- Images Section Start -->
        <div class="row">
            <div class="col-md-12">
            <div id="gallery">
                <div id="portfolio_btns">
                    <button class=" btn filter btn-primary" data-filter="all">ALL</button>
                    <button class="btn filter btn-primary" data-filter=".category-1">SUNDAY SERVICES</button>
                    <button class=" btn filter btn-primary" data-filter=".category-2">RADICAL KESHAS</button>
                </div>

                    <div class="row">
                        <div class="col-12">
                    <div class="mix category-1" data-my-order="1">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md10.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md10.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md11.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md11.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md12.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md12.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md13.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md13.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md14.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md14.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md15.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md15.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md16.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md16.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md17.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md17.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md18.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md18.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md19.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md19.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md20.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md20.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md21.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md21.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md22.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md22.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md23.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md23.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md24.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md24.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md25.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md25.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md26.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md26.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md27.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md27.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md28.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md28.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md29.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md29.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md30.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md30.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md32.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md32.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md33.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md33.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md34.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md34.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md35.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md35.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md36.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md36.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md37.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md37.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md38.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md38.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md39.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md39.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md40.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md40.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md41.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md41.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md42.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md42.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md43.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md43.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md44.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md44.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md45.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md45.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md46.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md46.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md47.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md47.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md48.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md48.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md49.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md49.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/md50.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/md50.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-2" data-my-order="3">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa1.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa1.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-2" data-my-order="2">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa2.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa2.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-2" data-my-order="5">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa3.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa3.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-2" data-my-order="6">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa4.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa4.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-2" data-my-order="8">
                        <div class="col-md-3 col-sm-6 col-xs-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa5.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa5.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-2" data-my-order="8">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa6.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa6.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="8">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa7.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa7.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-1" data-my-order="8">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa8.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa8.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-2" data-my-order="8">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa9.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa9.jpg') }}" class="img-fluid"> </div>
                    </div>
                    <div class="mix category-2" data-my-order="8">
                        <div class="col-md-3 col-sm-6 col-6">
                            <a class="fancybox" href="{{ asset('assets/images/gallery/ssa10.jpg') }}"><i class="fa fa-search-plus"></i></a>
                            <a href="{{ URL::to('portfolioitem') }}"><i class="fa fa-link"></i></a>
                        </div>
                        <div class="thumb_zoom"><img src="{{ asset('assets/images/gallery/ssa10.jpg') }}" class="img-fluid"> </div>
                    </div>
                </div>
                    </div>
                </div>
            </div>
        </div>
        </div>

        <!-- //Images Section End -->
    <!-- Container Section End -->
@stop

{{-- page level scripts --}}
@section('footer_scripts')
    <!-- page level js starts-->
    <script type="text/javascript" src="{{ asset('assets/vendors/mixitup/mixitup.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/frontend/portfolio.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/jquery.mixitup.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('assets/vendors/fancybox/js/jquery.mousewheel.pack.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('assets/vendors/fancybox/js/jquery.fancybox.pack.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('assets/vendors/fancybox/js/jquery.fancybox-buttons.js') }}"></script>
<script type="text/javascript"
        src="{{ asset('assets/vendors/fancybox/js/jquery.fancybox-thumbs.js') }}"></script>
<!-- Add Media helper (this is optional) -->
<script type="text/javascript"
        src="{{ asset('assets/vendors/fancybox/js/jquery.fancybox-media.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/pages/gallery.js') }}"></script>

    <!--page level js ends-->

@stop
